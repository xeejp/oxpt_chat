defmodule Oxpt.Chat.Guest do
  @moduledoc """
  Documentation for Oxpt.Chat.Guest
  """

  use Cizen.Automaton
  defstruct [:room_id]

  use Cizen.Effects
  use Cizen.Effectful
  alias Cizen.{Event, Filter}
  alias Oxpt.JoinGame
  alias Oxpt.Persistence
  alias Oxpt.Game

  alias Oxpt.Chat.{Host, Locales}

  alias Oxpt.Chat.Events.{
    FetchState,
    UpdateStateSome,
    UpdateStateAll,
    Add,
    Edit,
    Remove
  }

  use Oxpt.Game,
    root_dir: Path.join(__ENV__.file, "../../guest") |> Path.expand()

  @impl Oxpt.Game
  def player_socket(game_id, %__MODULE__{room_id: _room_id}, guest_id) do
    %__MODULE__.PlayerSocket{game_id: game_id, guest_id: guest_id}
  end

  @impl Oxpt.Game
  def new(room_id, _params) do
    %__MODULE__{room_id: room_id}
  end

  @impl Oxpt.Game
  def metadata(),
    do: %{
      label: "label_chat",
      category: "category_other"
    }

  @impl true
  def spawn(id, %__MODULE__{room_id: room_id}) do
    Persistence.Game.setup(id, room_id)

    host_game_id =
      unless Application.get_env(:oxpt, :restoring, false) do
        perform(id, %Start{saga: Host.new(room_id, game_id: id)})
      else
        nil
      end

    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %FetchState{game_id: ^id}
          } ->
            true

          %Event{
            body: %JoinGame{game_id: ^id}
          } ->
            true

          %Event{
            body: %Add{game_id: ^id}
          } ->
            true

          %Event{
            body: %Edit{game_id: ^id}
          } ->
            true

          %Event{
            body: %Remove{game_id: ^id}
          } ->
            true
        end)
    })

    initial_state = %{
      host_game_id: host_game_id,
      host_guest_id: nil,
      guest_game_id: id,
      players: %{},
      locales: Locales.get(),
      chat_log: []
    }

    {:loop, initial_state}
  end

  @impl true
  def yield(id, {:loop, state}) do
    event = perform(id, %Receive{})

    state = handle_event_body(id, event.body, state)
    {:loop, state}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id, host: true}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Request{
      body: %JoinGame{game_id: state.host_game_id, guest_id: guest_id, host: true}
    })

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    %{new_state | host_guest_id: guest_id}
  end

  defp handle_event_body(id, %JoinGame{guest_id: guest_id}, state) do
    player = new_player(state)
    new_state = put_in(state, [:players, guest_id], player)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          players: get_in(new_state, [:players])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %FetchState{guest_id: guest_id}, state) do
    perform(id, %Dispatch{
      body: %UpdateStateSome{
        game_id: id,
        guest_ids: [guest_id],
        state: state
      }
    })

    state
  end

  defp handle_event_body(id, %Add{guest_id: guest_id, payload: payload}, state) do
    new_state =
      state
      |> update_in([:chat_log], fn log ->
        [%{
          date: payload["date"],
          message: payload["message"],
          owner_name: payload["owner_name"],
          owner_id: guest_id,
          key: payload["key"]
        } | log]
      end)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          chat_log: get_in(new_state, [:chat_log])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %Edit{guest_id: _guest_id, payload: payload}, state) do
    new_state =
      state
      |> update_in([:chat_log], fn log ->
        Enum.map(log, fn item ->
          if item.key == payload["key"] do
            put_in(item, [:message], payload["message"])
          else
            item
          end
        end)
      end)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          chat_log: get_in(new_state, [:chat_log])
        }
      }
    })

    new_state
  end

  defp handle_event_body(id, %Remove{guest_id: _guest_id, payload: payload}, state) do
    new_state =
      state
      |> update_in([:chat_log], fn log ->
        Enum.filter(log, fn item -> item.key != payload["key"] end)
      end)

    perform(id, %Dispatch{
      body: %UpdateStateAll{
        game_id: id,
        state: %{
          chat_log: get_in(new_state, [:chat_log])
        }
      }
    })

    new_state
  end

  defp new_player(_state) do
    %{
      name: ""
    }
  end
end
