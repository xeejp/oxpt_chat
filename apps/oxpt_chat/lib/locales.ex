defmodule Oxpt.Chat.Locales do
  def get do
    %{
      en: %{
        translations: %{
          variables: %{
          },
          guest: %{
            experiment: %{
              title_01: "Display name",
              enter_name_01: "Please enter your new display name here.",
              cancel_01: "Cancel",
              rename_01: "Rename",
              display_name_01: "Display Name",
              message_01: "Message",
              submit_01: "Submit",
              edit_01: "Edit",
              remove_message_01: "Remove Message",
              remove_ok_01: "Are you sure you want to remove this message?",
              remove_01: "Remove"
            },
          }
        }
      },
      ja: %{
        translations: %{
          variables: %{
          },
          guest: %{
            experiment: %{
              title_01: "表示名",
              enter_name_01: "あなたの名前を入力してください。",
              cancel_01: "キャンセル",
              rename_01: "変更",
              display_name_01: "名前",
              message_01: "メッセージ",
              submit_01: "送信",
              edit_01: "編集",
              remove_message_01: "メッセージの削除",
              remove_ok_01: "本当にメッセージを削除しますか？",
              remove_01: "削除"
            },
          }
        }
      }
    }
  end
end
