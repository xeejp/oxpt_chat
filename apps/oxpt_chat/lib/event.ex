defmodule Oxpt.Chat.Events do
  @moduledoc """
  Documentation for Oxpt.Chat.Events

  gameで使うイベントを定義する
  """

  defmodule UpdateStateAll do
    @moduledoc """
    すべてのplayer_socketがsubscribeしておくべきイベント。
    全員に対しあるstateを送りたいときに使う

    """
    defstruct [:game_id, :state]
  end

  defmodule UpdateStateSome do
    @moduledoc """
    guest_idsで指定されたゲストのplayer_socketがsubscribeしておくべきイベント。
    そのゲストに対しあるstateを送りたいときに使う

    """
    defstruct [:guest_ids, :game_id, :state]
  end

  defmodule FetchState do
    @moduledoc """
    stateを管理しているオートマトンがsubscribeしておくべきイベント。
    player_socketがspawnした時にDispatchし、クライアントにstateを送る

    """
    defstruct [:game_id, :guest_id]
  end

  defmodule Add do
    defstruct [:game_id, :guest_id, :payload]
  end

  defmodule Edit do
    defstruct [:game_id, :guest_id, :payload]
  end

  defmodule Remove do
    defstruct [:game_id, :guest_id, :payload]
  end
end
