defmodule Oxpt.Chat.Guest.PlayerSocket do
  use Cizen.Automaton
  defstruct [:game_id, :guest_id]

  use Cizen.Effects
  alias Cizen.{Filter, Event}
  alias Oxpt.Player.{Input, Output}

  alias Oxpt.Chat.Events.{
    UpdateStateSome,
    UpdateStateAll,
    FetchState,
    Add,
    Edit,
    Remove
  }

  @impl true
  def spawn(id, %__MODULE__{} = socket) do
    perform(id, %Subscribe{
      event_filter:
        Filter.new(fn
          %Event{
            body: %UpdateStateAll{game_id: ^socket.game_id}
          } ->
            true

          %Event{
            body: %UpdateStateSome{game_id: ^socket.game_id, guest_ids: ids}
          } ->
            Enum.member?(ids, socket.guest_id)

          %Event{
            body: %Input{event: "add", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "edit", guest_id: ^socket.guest_id}
          } ->
            true

          %Event{
            body: %Input{event: "remove", guest_id: ^socket.guest_id}
          } ->
            true
        end)
    })

    perform(id, %Dispatch{
      body: %FetchState{
        game_id: socket.game_id,
        guest_id: socket.guest_id
      }
    })

    {:loop, socket}
  end

  @impl true
  def yield(id, {:loop, socket}) do
    event = perform(id, %Receive{})

    case event.body do
      %Input{} = input ->
        handle_input(id, input, socket)

      %UpdateStateAll{state: state} ->
        dispatch_state(id, state, socket)

      %UpdateStateSome{state: state} ->
        dispatch_state(id, state, socket)
    end

    {:loop, socket}
  end

  defp handle_input(id, %Input{event: "add", payload: payload}, socket) do
    perform(id, %Dispatch{
      body: %Add{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        payload: payload
      }
    })
  end

  defp handle_input(id, %Input{event: "edit", payload: payload}, socket) do
    perform(id, %Dispatch{
      body: %Edit{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        payload: payload
      }
    })
  end

  defp handle_input(id, %Input{event: "remove", payload: payload}, socket) do
    perform(id, %Dispatch{
      body: %Remove{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        payload: payload
      }
    })
  end


  defp dispatch_state(id, state, socket) do
    perform(id, %Dispatch{
      body: %Output{
        game_id: socket.game_id,
        guest_id: socket.guest_id,
        event: "update_state",
        payload: %{state: get_merge_data(state, socket.guest_id)}
      }
    })
  end

  defp get_merge_data(state, guest_id) do
    player = get_in(state, [:players, guest_id]) || %{}

    state
    |> Map.merge(player)
    |> Map.merge(%{
      guest_id: guest_id
    })
  end

end
