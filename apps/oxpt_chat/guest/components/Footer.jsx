import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'

import i18nInstance from '../i18n'
import Guest from './Guest'

const useStyles = makeStyles(theme => ({
  button: {
    marginLeft: theme.spacing(1)
  },
  chip: {
    margin: theme.spacing(1)
  },
  form: {
    width: '100%',
    margin: '1rem',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  }
}))

const Footer = ({
  name,
  setOpen,
  handleSubmit,
  message,
  setMessage,
  editItem,
  setEditItem
}) => {
  const classes = useStyles()
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <form className={classes.form} onSubmit={handleSubmit}>
      <Guest className={classes.chip} name={name} setOpen={setOpen} />
      <TextField
        label={t('guest.experiment.message_01')}
        fullWidth
        id="text-field"
        autoComplete="off"
        className={classes.textField}
        value={message}
        onChange={e => setMessage(e.target.value)}
        margin="dense"
        variant="outlined"
      />
      {editItem && (
        <Button
          onClick={() => {
            setEditItem(null)
            setMessage('')
          }}
          color="secondary"
          className={classes.button}
        >
          {t('guest.experiment.cancel_01')}
        </Button>
      )}
      <Button color="primary" className={classes.button} type="submit">
        {editItem ? t('guest.experiment.edit_01') : t('guest.experiment.submit_01')}
      </Button>
    </form>
  )
}

Footer.propTypes = {
  name: PropTypes.string,
  setOpen: PropTypes.func,
  handleSubmit: PropTypes.func,
  message: PropTypes.string,
  setMessage: PropTypes.func,
  editItem: PropTypes.string,
  setEditItem: PropTypes.func
}

export default Footer
