import React, { useReducer, useEffect, useState } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import TextField from '@material-ui/core/TextField'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'

import i18nInstance from '../i18n'

const RenameDialog = ({ setOpen, open, setName, name }) => {
  const { locales } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const [temp, setTemp] = useState(name)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  useEffect(() => {
    setTemp(name)
  }, [open])

  if (!(locales))
    return <></>

  return (
    <div>
      <Dialog
        open={open}
        onClose={() => setOpen(false)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {t('guest.experiment.title_01')}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>{t('guest.experiment.enter_name_01')}</DialogContentText>
          <TextField
            autoFocus
            margin="dense"
            id="name"
            label={t('guest.experiment.display_name_01')}
            value={temp}
            onChange={e => setTemp(e.target.value)}
            fullWidth
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setOpen(false)} color="primary">
            {t('guest.experiment.cancel_01')}
          </Button>
          <Button
            onClick={() => {
              if (temp !== '')
                setName(temp)
              setOpen(false)
            }}
            color="primary"
          >
            {t('guest.experiment.rename_01')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

RenameDialog.propTypes = {
  setOpen: PropTypes.func,
  open: PropTypes.bool,
  setName: PropTypes.func,
  name: PropTypes.string
}

export default RenameDialog
