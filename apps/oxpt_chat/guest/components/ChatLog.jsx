import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import Typography from '@material-ui/core/Typography'
import Chip from '@material-ui/core/Chip'
import IconButton from '@material-ui/core/IconButton'
import DeleteIcon from '@material-ui/icons/Delete'
import EditIcon from '@material-ui/icons/Edit'

import i18nInstance from '../i18n'
import { getColor, formatDate } from '../utils'

const useStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    overflow: 'auto',
    height: `calc(100vh - (${theme.mixins.toolbar.minHeight}px + 40px))`
  },
  name: {
    display: 'block',
    marginBottom: 8,
    marginLeft: 8
  },
  date: {
    display: 'block',
    margin: 8
  }
}))

const ChatLog = ({ setRemoveItem, handleEdit }) => {
  const classes = useStyles()
  const { locales, chatLog, guestId } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  return (
    <List id="chat_view" className={classes.root}>
      {chatLog.map(({ date, key, message, owner_id: ownerId, owner_name: ownerName }) => {
        const own = ownerId === guestId
        return (
          <ListItem
            style={own ? { textAlign: 'right' } : {}}
            alignItems="flex-start"
            key={key}
          >
            <ListItemText
              primary={
                <React.Fragment>
                  {!own && (
                    <Typography
                      component="span"
                      variant="body1"
                      className={classes.name}
                      color="textPrimary"
                    >
                      {ownerName}
                    </Typography>
                  )}
                  <Chip
                    label={message}
                    style={{ backgroundColor: getColor(ownerId) }}
                  />
                </React.Fragment>
              }
              secondary={
                <Typography
                  component="span"
                  variant="body2"
                  className={classes.date}
                  color="textSecondary"
                >
                  {formatDate(new Date(date), 'hh:mm:ss')}
                  {own && (
                    <>
                      <IconButton
                        onClick={() => handleEdit(key)}
                        edge="end"
                        aria-label="Edit"
                      >
                        <EditIcon fontSize="small" />
                      </IconButton>
                      <IconButton
                        onClick={() => setRemoveItem(key)}
                        edge="end"
                        aria-label="Delete"
                      >
                        <DeleteIcon fontSize="small" />
                      </IconButton>
                    </>
                  )}
                </Typography>
              }
            />
          </ListItem>
        )
      }).reverse()}
    </List>
  )
}

ChatLog.propTypes = {
  setRemoveItem: PropTypes.func,
  handleEdit: PropTypes.func
}

export default ChatLog
