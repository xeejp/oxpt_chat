import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles, useTheme } from '@material-ui/core/styles'
import Fab from '@material-ui/core/Fab'
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward'
import Zoom from '@material-ui/core/Zoom'

import i18nInstance from '../i18n'

const useStyles = makeStyles(theme => ({
  fab: {
    margin: 'auto',
    position: 'fixed',
    bottom: 100,
    left: 0,
    right: 0
  }
}))

const SubscribeButton = ({ subscribe, handleSubscribe }) => {
  const classes = useStyles()
  const theme = useTheme()
  const { locales } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen
  }

  if (!(locales))
    return <></>

  return (
    <Zoom
      in={!subscribe}
      timeout={transitionDuration}
      style={{
        transitionDelay: `${!subscribe ? transitionDuration.exit : 0}ms`
      }}
      unmountOnExit
    >
      <Fab
        onClick={() => handleSubscribe()}
        color="primary"
        aria-label="Sync"
        className={classes.fab}
      >
        <ArrowDownwardIcon />
      </Fab>
    </Zoom>
  )
}

SubscribeButton.propTypes = {
  subscribe: PropTypes.any,
  handleSubscribe: PropTypes.any
}

export default SubscribeButton
