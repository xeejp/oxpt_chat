import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import Button from '@material-ui/core/Button'

import i18nInstance from '../i18n'

const RemoveDialog = ({ setRemoveItem, removeItem }) => {
  const { locales, chatLog, pushState } = useStore()

  const [t, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  const item = chatLog.find(({ key }) => key === removeItem)

  if (!(locales && item))
    return <></>

  return (
    <div>
      <Dialog
        open={true}
        onClose={() => setRemoveItem(null)}
        aria-labelledby="form-dialog-title"
      >
        <DialogTitle id="form-dialog-title">
          {t('guest.experiment.remove_message_01')}
        </DialogTitle>
        <DialogContent>
          <DialogContentText>{t('guest.experiment.remove_ok_01')}</DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => setRemoveItem(null)} color="primary">
            {t('guest.experiment.cancel_01')}
          </Button>
          <Button
            onClick={() => {
              pushState({ event: 'remove',
                payload: {
                  key: removeItem
                }})
              setRemoveItem(null)
            }}
            color="secondary"
          >
            {t('guest.experiment.remove_01')}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  )
}

RemoveDialog.propTypes = {
  setRemoveItem: PropTypes.func,
  removeItem: PropTypes.string
}

export default RemoveDialog
