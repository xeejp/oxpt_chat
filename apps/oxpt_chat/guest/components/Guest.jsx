import React, { useReducer, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useStore } from '../actions/hook'
import { useTranslation } from 'react-i18next'

import PersonIcon from '@material-ui/icons/Person'
import EditIcon from '@material-ui/icons/Edit'
import Chip from '@material-ui/core/Chip'

import i18nInstance from '../i18n'

const Guest = ({ name, setOpen, className, size }) => {
  const { locales } = useStore()

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  if (!(locales))
    return <></>

  const opts = setOpen
    ? {
      onDelete: () => setOpen(true),
      deleteIcon: <EditIcon />
    }
    : {}

  return (
    <Chip
      icon={<PersonIcon />}
      label={name}
      color="primary"
      {...opts}
      className={className}
      variant="outlined"
      size={size}
    />
  )
}

Guest.propTypes = {
  name: PropTypes.string,
  setOpen: PropTypes.func,
  className: PropTypes.string,
  size: PropTypes.number
}

export default Guest
