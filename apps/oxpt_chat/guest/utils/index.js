import * as colors from "@material-ui/core/colors"

function hash(string) {
  let hash = 0,
    i,
    chr;
  if (string.length === 0) return hash;
  for (i = 0; i < string.length; i++) {
    chr = string.charCodeAt(i);
    hash = (hash << 5) - hash + chr;
    hash |= 0;
  }
  return hash < 0 ? -hash : hash;
}

export function getColor(guestId, shade = 200) {
  const palette = [
    colors.amber[shade],
    colors.blue[shade],
    colors.blueGrey[shade],
    colors.brown[shade],
    colors.common[shade],
    colors.cyan[shade],
    colors.deepOrange[shade],
    colors.deepPurple[shade],
    colors.green[shade],
    colors.grey[shade],
    colors.indigo[shade],
    colors.lightBlue[shade],
    colors.lightGreen[shade],
    colors.lime[shade],
    colors.orange[shade],
    colors.pink[shade],
    colors.purple[shade],
    colors.red[shade],
    colors.teal[shade],
    colors.yellow[shade]
  ];

  return palette[hash(guestId) % palette.length];
}

// https://qiita.com/osakanafish/items/c64fe8a34e7221e811d0
export function formatDate(date, format) {
  if (!format) format = "YYYY-MM-DD hh:mm:ss.SSS";
  format = format.replace(/YYYY/g, date.getFullYear());
  format = format.replace(/MM/g, ("0" + (date.getMonth() + 1)).slice(-2));
  format = format.replace(/DD/g, ("0" + date.getDate()).slice(-2));
  format = format.replace(/hh/g, ("0" + date.getHours()).slice(-2));
  format = format.replace(/mm/g, ("0" + date.getMinutes()).slice(-2));
  format = format.replace(/ss/g, ("0" + date.getSeconds()).slice(-2));
  if (format.match(/S/g)) {
    const milliSeconds = ("00" + date.getMilliseconds()).slice(-3);
    const length = format.match(/S/g).length;
    for (let i = 0; i < length; i++)
      format = format.replace(/S/, milliSeconds.substring(i, i + 1));
  }
  return format;
}

const toCamel = (str) => (
  str.replace(/([-_]\w)/g, g => g[1].toUpperCase())
)

const toSnake = (str) => (
  str.replace(/[A-Z]/g, g => `_${g[0].toLowerCase()}`)
)

const isNumber = function(value) {
  return ((typeof value === 'number') && (isFinite(value)))
}

export const fromSnakeToCamel = (obj) => {
  if (typeof obj !== 'object' || obj === null) {
    return obj
  } else {
    const result = {}
    Object.keys(obj).forEach(key => {
      if (obj[key] instanceof Object && !(obj[key] instanceof Array) && key !== 'locales' && key !== 'players') {
        result[toCamel(key)] = fromSnakeToCamel(obj[key])
      } else {
        result[toCamel(key)] = obj[key]
      }
    })
    return result
  }
}

export const fromCamelToSnake = (obj) => {
  if (typeof obj !== 'object' || obj === null) {
    return obj
  } else {
    const result = {}
    Object.keys(obj).forEach(key => {
      if (obj[key] instanceof Object && !(obj[key] instanceof Array) && key !== 'localesTemp') {
        result[toSnake(key)] = fromCamelToSnake(obj[key])
      } else {
        result[toSnake(key)] = obj[key]
      }
    })
    return result
  }
}
