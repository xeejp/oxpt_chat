import React, { useReducer, useEffect, useState } from 'react'
import { useStore } from './actions/hook'
import { useTranslation } from 'react-i18next'

import { makeStyles } from '@material-ui/core/styles'
import Grid from '@material-ui/core/Grid'

import i18nInstance from './i18n'
import RenameDialog from './components/RenameDialog'
import RemoveDialog from './components/RemoveDialog'
import Footer from './components/Footer'
import ChatLog from './components/ChatLog'
import SubscribeButton from './components/SubscribeButton'

const useStyles = makeStyles(theme => ({
  items: {
    padding: theme.spacing(1)
  },
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: 'auto!important',
    minHeight: '100%'
  },
  fab: {
    margin: 'auto',
    position: 'fixed',
    bottom: 100,
    left: 0,
    right: 0
  },
  footWrapper: {
    position: 'fixed',
    bottom: 0,
    left: 0,
    right: 0
  }
}))

export default () => {
  const classes = useStyles()
  const { locales, chatLog, guestId, pushState } = useStore()

  const [openRename, setOpenRename] = useState(true)
  const [removeItem, setRemoveItem] = useState(null)
  const [editItem, setEditItem] = useState(null)
  const [name, setName] = useState('Guest')
  const [message, setMessage] = useState('')
  const [subscribe, setSubscribe] = useState(true)

  const [, i18n] = useTranslation('translations', { i18nInstance })
  const [, forceUpdate] = useReducer(x => x + 1, 0)

  const scrollListener = () => {
    const view = document.getElementById('chat_view')
    if (view !== null) {
      const subscribe = view.scrollTop === view.scrollHeight - view.clientHeight
      setSubscribe(subscribe)
    }
  }

  const handleSubscribe = () => {
    const view = document.getElementById('chat_view')
    if (view !== null) view.scrollTop = view.scrollHeight - view.clientHeight
    setSubscribe(true)
  }

  useEffect(() => {
    locales && Object.keys(locales).map(lang => {
      Object.keys(locales[lang]).map(namespace => {
        i18n.addResourceBundle(lang, namespace, locales[lang][namespace])
      })
    })
    forceUpdate()
  }, [locales])

  useEffect(() => {
    const view = document.getElementById('chat_view')
    if (view !== null) {
      view.addEventListener('scroll', scrollListener)
      return () => {
        view.removeEventListener('scroll', scrollListener)
      }
    }
  })

  useEffect(() => {
    if (subscribe) {
      handleSubscribe()
    }
  }, [chatLog])

  if (!(locales))
    return <></>

  const handleSubmit = event => {
    event.preventDefault()
    window.scrollTo(0, 0)
    document.getElementById('text-field').onfocus = () => {
      blur()
    }
    setEditItem(null)
    if (message === '') return

    if (editItem) {
      pushState({ event: 'edit',
        payload: {
          message,
          key: editItem
        }})
      setEditItem(null)
    } else {
      const date = new Date().toJSON()
      pushState({ event: 'add',
        payload: {
          date,
          message,
          owner_name: name,
          key: date + guestId
        }})
    }
    setMessage('')
  }

  const handleEdit = (editItemKey) => {
    const item = chatLog.find(({ key }) => key === editItemKey)
    setMessage(item.message)
    setEditItem(editItemKey)
    document.getElementById('text-field').focus()
  }

  return (
    <div className={classes.container}>
      <ChatLog
        setRemoveItem={setRemoveItem}
        handleEdit={handleEdit}
      />
      <Grid
        className={classes.footWrapper}
        container
        direction="row"
        justify="space-between"
        alignItems="flex-start"
      >
        <Footer
          name={name}
          setOpen={setOpenRename}
          handleSubmit={handleSubmit}
          message={message}
          setMessage={setMessage}
          editItem={editItem}
          setEditItem={setEditItem}
        />
      </Grid>
      <SubscribeButton
        subscribe={subscribe}
        handleSubscribe={handleSubscribe}
      />
      <RenameDialog
        open={openRename}
        setOpen={setOpenRename}
        name={name}
        setName={setName}
      />
      <RemoveDialog
        removeItem={removeItem}
        setRemoveItem={setRemoveItem}
      />
    </div>
  )
}
